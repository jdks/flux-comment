var Dispatcher = require('flux').Dispatcher;
var assign = require('lodash').assign;

var CommentDispatcher = new Dispatcher();

CommentDispatcher.handleViewAction = function(action) {
  this.dispatch({
    source: 'VIEW_ACTION',
    action: action
  });
};

module.exports = CommentDispatcher;
