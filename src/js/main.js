/** @jsx React.DOM **/

var App = require('./components/app'),
    React = require('react');

React.render(
  <App />,
  document.getElementById('main')
);
