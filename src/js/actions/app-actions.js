var AppConstants = require('../constants/app-constants'),
    CommentDispatcher = require('../dispatchers/comment-dispatcher');

var AppActions = {
  addComment: function(comment) {
    CommentDispatcher.handleViewAction({
      actionType: AppConstants.ADD_COMMENT,
      comment: comment
    });
  }
};

module.exports = AppActions;
