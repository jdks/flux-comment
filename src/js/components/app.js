/** @jsx React.DOM **/

var React = require('react'),
    Comments = require('./comments'),
    CommentForm = require('./comment-form');

var App = React.createClass({
  render: function() {
    return (
      <div>
        <h1>Comments with Flux</h1>
        <Comments />
        <CommentForm />
      </div>
    );
  }
});

module.exports = App;
