/** @jsx React.DOM **/

var React = require('react'),
    AppActions = require('../actions/app-actions'),
    CommentStore = require('../stores/comment-store');

function _getComments() {
  return { comments: CommentStore.getComments() };
}

var Comments = React.createClass({
  getInitialState: function() {
    return _getComments();
  },
  componentWillMount: function() {
    CommentStore.addChangeListener(this.handleChange);
  },
  handleChange: function() {
    this.setState(_getComments());
  },
  render: function() {
    var comments = this.state.comments.map(function(comment, id) {
      return (
        <article key={id} className="panel radius row comment">
          <div className="large-9 columns">
            {comment.message}
          </div>
          <div className="large-3 columns">
            <a className="th">
              <img src="http://placekitten.com/g/200/200"></img>
            </a>
          </div>
          <div className="signature">
            {comment.name}
          </div>
        </article>
      );
    }).reverse();
    return (
      <div className="large-6 columns comments">
        <h2>Comments</h2>
        {comments}
      </div>
    );
  }
});

module.exports = Comments;
