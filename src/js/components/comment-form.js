/** @jsx React.DOM **/

var React = require('react'),
    AppActions = require('../actions/app-actions');

var CommentForm = React.createClass({
  _defaultComment: {
    message: '',
    name: '',
    email: ''
  },
  _focusOnMessage: function() { this.refs.message.getDOMNode().focus(); },
  getInitialState: function() { return this._defaultComment; },
  handleMessageChange: function(e) {
    this.setState({ message: e.target.value });
  },
  handleNameChange: function(e) {
    this.setState({ name: e.target.value });
  },
  handleEmailChange: function(e) {
    this.setState({ email: e.target.value });
  },
  handleSubmit: function() {
    var comment = {
      message: this.state.message,
      name: this.state.name,
      email: this.state.email
    };

    AppActions.addComment(comment);    
    this.clearAndFocus();
  },
  clearAndFocus: function() {
    this.setState(this._defaultComment, this._focusOnMessage);
  },
  render: function() {
    return (
      <div className="large-5 columns">
        <div className="row">
          <h2>Write Something</h2>
          <form>

            <div className="row message">
              <div className="large-12 columns">
                <textarea
                  ref="message"
                  placeholder="Write your message here..."
                  value={this.state.message}
                  onChange={this.handleMessageChange}
                />
              </div>
            </div>

            <div className="row commenter">
              <div className="large-6 columns name">
                <label>name</label>
                <input
                  ref="name"
                  type="text"
                  placeholder="John Doe"
                  value={this.state.name}
                  onChange={this.handleNameChange}
                />
              </div>
              <div className="large-6 columns email">
                <label>email address</label>
                <input
                  ref="email"
                  type="text"
                  placeholder="john.doe@example.com"
                  value={this.state.email}
                  onChange={this.handleEmailChange}
                />
              </div>
            </div>

            <div className="row">
              <div className="large-12 columns">
                <a  className="button radius large"
                    onClick={this.handleSubmit} >
                  Leave your comment
                </a>
              </div>
            </div>

          </form>
        </div>
      </div>
    );
  }
});

module.exports = CommentForm;
