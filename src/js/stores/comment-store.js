var CommentDispatcher = require('../dispatchers/comment-dispatcher'),
    AppConstants = require('../constants/app-constants'),
    EventEmitter = require('events').EventEmitter,
    assign = require('lodash').assign;

var CHANGE_EVENT = 'change';

var _comments = [];

function _addComment(comment) { _comments.push(comment); }

var CommentStore = assign(EventEmitter.prototype, {
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },
  addChangeListener: function(listener) {
    this.on(CHANGE_EVENT, listener);
  },
  removeChangeListener: function(listener) {
    this.removeListener(CHANGE_EVENT, listener);
  },
  getComments: function() {
    return _comments;
  },
  dispatchToken: CommentDispatcher.register(function(payload) {
    var action = payload.action;

    if (action.actionType === AppConstants.ADD_COMMENT) {
      _addComment(action.comment);
    }
        
    CommentStore.emitChange();

    return true;
  })
});

module.exports = CommentStore;
