var gulp = require('gulp'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    livereload = require('gulp-livereload'),
    fork = require('child_process').spawn;

gulp.task('browserify', function() {
  gulp.src('src/js/main.js')
    .pipe(browserify({transform: 'reactify'}))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/js'));
});

gulp.task('build', function() {
  gulp.src('src/index.html')
    .pipe(gulp.dest('dist'));
});

gulp.task('serve', function() {
  fork('python', ['-m', 'SimpleHTTPServer'], {
    cwd: process.cwd() + '/dist'
  });
});

gulp.task('watch', ['serve'], function() {
  livereload.listen();
  gulp.watch('src/**', ['default']);
  gulp.watch('dist/**')
    .on('change', livereload.changed);
});

gulp.task('default', ['browserify', 'build']);
